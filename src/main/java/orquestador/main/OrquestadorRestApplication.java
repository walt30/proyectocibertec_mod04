package orquestador.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrquestadorRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrquestadorRestApplication.class, args);
	}

}
