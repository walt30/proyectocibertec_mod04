package orquestador.main.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import orquestador.main.entity.Person;

@RestController
public class PruebaCOntroller {

	@GetMapping(path = "/hola", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Person prueba() {
		return new Person("walter", 25);
	}

	
	@PostMapping(path = "/dni", consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Person validacionDNI(@RequestBody Person persona) {
		System.out.println(persona.toString());
		return new Person("walter", 25);
	}
}